# sbt

Interactive build tool for Scala, Java, and more. [scala-sbt.org](https://scala-sbt.org)

# Install
## Select a java (jre) version
* Linux distribution do not always require the best version.
* [*Installing sbt on Linux*
  ](https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html)
> This version of Scala is fixed for a specific sbt release and cannot be changed. For sbt 1.3.4, this version is Scala 2.12.10. *Configuring Scala*/[*sbt’s Scala version*
](https://www.scala-sbt.org/1.x/docs/Configuring-Scala.html#sbt%E2%80%99s+Scala+version)

# Add a library dependency
* *sbt by example*/[*Add a library dependency*
  ](https://www.scala-sbt.org/1.x/docs/sbt-by-example.html#Add+a+library+dependency)

# Documentation
* sbt [1](https://manpages.debian.org/unstable/sbt/sbt.1.en.html)